$(document).ready(function() {
    $('.accordion')
        .on('show.bs.collapse', function(e) {
            $(e.target).siblings('.card-header').find(".fa-arrow-down").addClass("rotated");
        })
        .on('hide.bs.collapse', function(e) {
            $(e.target).siblings('.card-header').find(".fa-arrow-down").removeClass("rotated");
        });
});