jQuery(document).ready(function(){
	jQuery(".mainscroll").innerHeight(window.innerHeight);
	init_jquery();
});



function init_jquery(){

	jQuery(".main__scrbt").unbind('click');
	jQuery(".mapa__desc").unbind('click');
	jQuery("#reproductor").unbind('click');
	jQuery(".togglereg").unbind('click');
	jQuery(".faq__title").unbind('click');


	jQuery(".seccion").each(function(){
		if(jQuery(this).find(".texto").length > 0){
			jQuery(this).addClass("seccion--texto");
		}
	});
	jQuery(".main__scrbt").click(function () {
		jQuery('html,body').animate({
			scrollTop: jQuery(".main").height()}, 'slow');
	});
	jQuery(".main-mapa__desc").click(function () {
		jQuery(".main-mapa").toggleClass("main-mapa__desc--toggled");
	});
	

	
	jQuery("#reproductor").on('click','.reproductor__button--play',function () {
		jQuery("body").toggleClass("playing");
		jQuery("body").addClass("miniplayer");

		if(jQuery("#reproductor_audio").length == 0){
			jQuery(".main__videocont").fadeToggle();
		}
		else{
			if(document.getElementById('reproductor_audio').paused){
				document.getElementById('reproductor_audio').play();
			}
			else{
				document.getElementById('reproductor_audio').pause();
			}
		}
			
	});

	jQuery("#reproductor").on('click','.reproductor__button--cerrar',function () {
		jQuery("body").toggleClass("playing");
		jQuery("body").removeClass("miniplayer");
		document.getElementById('reproductor_audio').pause();			
	});




	jQuery(".togglereg").click(function(){
		jQuery(".main--registro").fadeToggle();
		jQuery(".main--logeo").fadeToggle();
	});


	//FIX ALTURA
	if(jQuery('.portada').length<1){
		var altVent = jQuery(window).height();
		jQuery(".main").height(altVent);

	}
	


	var altVent2= jQuery(window).innerHeight();
	jQuery(".mainscroll").innerHeight(altVent2);

	window.addEventListener('resize',function(){
		var vh = window.innerHeight * 0.01;
		document.documentElement.style.setProperty('--vh',vh+'px');
	});


	// jQuery(".main,.mainscroll").height(altVent);
	
	// EFECTO FADE
	// window.sr = ScrollReveal({ reset: false, duration: 800, scale: 1, viewFactor: 0.4 });

	// 			var duration = 1600;
	// 			var esperar = 1350;
	// 			window.sr = ScrollReveal({ 
	// 				delay: esperar,
	// 				duration: duration,
	// 				opacity: 0, 
	// 				scale: 0,
	// 				distance: "500px",
	// 				reset: true,
	// 				viewFactor: 0,
	// 				useDelay:'always',
	// 				delay: 500,
	// 			});

	var duration = 800;
	var esperar = 200;
	window.sr = ScrollReveal({ 
		delay: esperar,
		duration: duration,
		opacity: 0.0, 
		scale: 1,
		distance: "10px",
		reset: false,
		viewFactor: 0,
		useDelay:'always',
	});


	// HISTORIAS
	sr.reveal('.desde-derecha>*', { 
		delay: esperar,
		origin: "rigth"
	});
	sr.reveal('.desde-izquierda>*', { 
		delay: esperar,
		origin: "left"
	});
	sr.reveal('.desde-arriba>*', { 
		delay: esperar,
		origin: "top"
	});
	sr.reveal('.desde-abajo>*', { 
		delay: esperar	,
		origin: "bottom"
	});


	// SCROLLS
	duration = 1500;
	esperar = 1000;
	sr.reveal('.anim-derecha', {
		delay: esperar,
		duration: duration,
		opacity: 0, 
		scale: 0,
		distance: "500px",
		reset: true,
		viewFactor: 0,
		useDelay:'always',
		origin: "right"
	});
	sr.reveal('.anim-izquierda', {
		delay: esperar,
		duration: duration,
		opacity: 0, 
		scale: 0,
		distance: "500px",
		reset: true,
		viewFactor: 0,
		useDelay:'always',
		origin: "left"
	});
	sr.reveal('.anim-abajo', {
		delay: esperar,
		duration: duration,
		opacity: 0, 
		scale: 0,
		distance: "500px",
		reset: true,
		viewFactor: 0,
		useDelay:'always',
		origin: "bottom"
	});
	sr.reveal('.anim-arriba', {
		delay: esperar,
		duration: duration,
		opacity: 0, 
		scale: 0,
		distance: "500px",
		reset: true,
		viewFactor: 0,
		useDelay:'always',
		origin: "top"
	});
	sr.reveal('.anim-mostralo', {
		delay: esperar,
		duration: duration,
		opacity: 0, 
		scale: 0,
		distance: "0px",
		reset: true,
		viewFactor: 0,
		useDelay:'always',
		origin: "bottom"
	});
	sr.reveal('.bloque__figure.anim-derecha', {
		delay: esperar,
		duration: duration,
		opacity: 0, 
		scale: 0,
		distance: "500px",
		reset: true,
		viewFactor: 0,
		useDelay:'always',
		origin: "right"
	});
	sr.reveal('.bloque__figure.anim-izquierda', {
		delay: esperar,
		duration: duration,
		opacity: 0, 
		scale: 0,
		distance: "500px",
		reset: true,
		viewFactor: 0,
		useDelay:'always',
		origin: "left"
	});

	//FAQ
	jQuery(".faq__title").click(function(){
		jQuery(this).parent().toggleClass("toggled");
	});
	//UBICACION DE PLAYER
	jQuery(window).scroll(function() {
		var scr =  jQuery(document).height() - (jQuery(window).scrollTop() + window.innerHeight);
		if(scr <= 55){
			jQuery(".reproductor").css("bottom", "55px");
		}
		else{
			jQuery(".reproductor").css("bottom", "0px");
		}
	});
	
	/*
				__       
	 |\/|  /\  |__)  /\  
	 |  | /~~\ |    /~~\ 
						 
	*/

	if(jQuery('#mapa').length>0){

		var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
				'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

		var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
			streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});

		var map = L.map('mapa', {
			center: [51.5, -0.09],
			zoom: 10,
			layers: [grayscale]
		});

		var greenIcon = L.icon({
		    iconUrl: 'assets/img/map-marker.svg',
		    iconSize:     [40, 40], // size of the icon
		    iconAnchor:   [30, 30] // point of the icon which will correspond to marker's location
		    // popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
		});

		var pane = map.createPane('fixed', document.getElementById('mapa'));
		var marker = L.marker([51.5, -0.09], {icon: greenIcon}).addTo(map);
		var marker2 = L.marker([51.61, -0.07], {icon: greenIcon}).addTo(map);

		var popup = L.popup({
		  pane: 'fixed', // Указываем нужный pane
		  className: 'popup-fixed',
		  autoPan: false,
		}).setContent('<p class="small mapa__ciudad"><i class="icon icon--mapinfo"></i>Fernando de la mora</p><h2 class="mapa__titulo">Techo caido en fdo de la mora</h2><p class="paragraph--1">Una bella baja corta consisa y en lo posible pegadiza. Dos lineas nomas. <a href="#">Ver ficha</a></p>');

		var popup2 = L.popup({
		  pane: 'fixed', // Указываем нужный pane
		  className: 'popup-fixed',
		  autoPan: false,
		}).setContent('<p class="small mapa__ciudad"><i class="icon icon--mapinfo"></i>Fernando de la mora</p><h2 class="mapa__titulo">Techo caido en Londres, Nueva Londres</h2><p class="paragraph--1">Una bella baja corta consisa y en lo posible pegadiza. Dos lineas nomas. <a href="#">Ver ficha</a></p>');

		marker.bindPopup(popup);
		marker2.bindPopup(popup2);
		var baseLayers = {
			"Grayscale": grayscale
		};

		L.control.layers(baseLayers).addTo(map);
		// alambre active a los markers
		jQuery(".leaflet-marker-icon.leaflet-zoom-animated.leaflet-interactive").click(function () {
			jQuery(".leaflet-marker-icon.leaflet-zoom-animated.leaflet-interactive").removeClass("marker-active");
			jQuery(".leaflet-marker-icon.leaflet-zoom-animated.leaflet-interactive").attr("src", "assets/img/map-marker.svg");
			jQuery(this).addClass("marker-active");
			jQuery(".main-mapa").parent().addClass("mappop");
			jQuery(this).attr("src", "assets/img/map-marker-active.svg");
		});
		jQuery(".mappop .leaflet-popup-close-button").click(function(){
			jQuery(".mappop").removeClass("mappop");
		});
	}

	jQuery('.raizgral_factcheck button.respuestas').click(function(){
		var self=this;

		var correcto=false;

		jQuery(self).css('color','white');
		jQuery(self).css('backgroundColor','black');
		if(jQuery(self).attr('correcto')=='t'){
			// jQuery(self).css('backgroundColor','rgb(81,184,72)');
			// jQuery(self).css('borderColor','rgb(81,184,72)');
			correcto=true;
		} else {
			// jQuery(self).css('backgroundColor','rgb(230,46,46)');
			// jQuery(self).css('borderColor','rgb(230,46,46)');
		}

		var msj=jQuery(self).text();
		var texto=jQuery('button.respuestas:first').parent().find('p:first').text();
		jQuery('button.respuestas').attr('disabled','disabled');

		if(correcto!=true){
			msj='FALLASTE 👎';
			// msj='<br>FALLASTE. HACÉ SCROLL PARA SABER LA PRECISA!';
		} else {
			msj='ACERTASTE 👍';
			// msj='<br>ESTAS EN LO CORRECTO. HACÉ SCROLL PARA SABER LA PRECISA!';
		}

		jQuery(this).html(msj);

		ga('send', 'event', 'Factcheck', msj, jQuery(self).parent().find('p:first').text());
		// jQuery('.main--4__contenidos p.small:first').html(msj);
	});


	jQuery('#btnsprev, #btnsnext').click(function(ev){
		var bgcolor=jQuery(this).attr('bgcolor');
		document.querySelector('body').className = '';
		document.querySelector('body').className = jQuery(this).attr('color');

		if(bgcolor=='transparent'){ bgcolor=''; }

		document.querySelector('body').style.backgroundColor=bgcolor;
	});

	jQuery('.videoplay').click(function(){
		jQuery('.main__videocont:first').fadeIn(600);
		// jQuery('.main__bgs:first').css("-webkit-transform","scale(1.2)").css("transform","scale(1.2)").fadeOut(1000,function(){
		// });

		// jQuery(".main__bgs:first img").animate({
		// 	transition: "all .6s ease-in-out",
		// 	transform: "scale(1.5)",
		// }, 5000, function() {
		// 	// Things to do when animation is complete.
		// });

	});

	jQuery('.main__videocont-cerrar').click(function(){
		jQuery('.main__videocont:first').fadeOut(300);
		// jQuery('.main__bgs:first img').fadeIn(600,function(){
		// });
	});

	share_url();

}


function share_url(){

	var URL=window.location.href;
	var DESC=document.title;

	var FB_SHARE='https://www.facebook.com/sharer/sharer.php?u='+URL;
	var TWT_SHARE='https://twitter.com/intent/tweet?text='+DESC+' '+URL;
	var TLGRM_SHARE='https://telegram.me/share/url?url='+URL+'&text='+DESC;
	var WA_SHARE='https://wa.me/?text='+DESC+' '+URL;

	jQuery('#share_facebook').attr('href',FB_SHARE);
	jQuery('#share_twitter').attr('href',TWT_SHARE);
	jQuery('#share_telegram').attr('href',TLGRM_SHARE);
	jQuery('#share_whatsapp').attr('href',WA_SHARE);

}


window.onload = function(){
console.log(`%c _________________________
< Por Vivi & Pablo con 💖 >
 -------------------------
        \\   ^__^
         \\  (oo)\\_______
            (__)\\       )\\/\\
                ||----w |
                ||     ||`, "font-family:monospace");
}
